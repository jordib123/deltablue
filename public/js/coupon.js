$(document).ready(function () {
	const socket = io("http://deltablue-4e6c473af66d.victhorious.com");

	socket.on('connect', () => {
		socket.emit('session_id', '123456');
	});

	socket.on('validated', () => {
		var before = document.getElementById("before");
		var after = document.getElementById("after");
		before.style.display = "none";
		after.style.display = "block";
	});
});
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const path = require('path');

const port = process.env.PORT || 8080;

app.use(express.static('public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/', function(req, res) {
    res.send('Hello world');
});

app.get('/validator', function(req, res) {
    res.sendFile(path.join(__dirname + '/validator.html'));
});

app.get('/coupon', function(req, res) {
    res.sendFile(path.join(__dirname + '/coupon.html'));
});

app.post('/api/validate', function (req, res) {
	var coupon_session = req.body.session_id;
	io.in(coupon_session).emit('validated');
});

io.on('connection', function (socket) {
	socket.on('session_id', function (data) {
		socket.join(data);
	});
});

// start the server
http.listen(3000, function () {
	console.log(`Server running.`);
});